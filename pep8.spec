%global _empty_manifest_terminate_build 0
%global common_desc \
	pep8 Python style guide checker pep8 is a tool to check your Python code
Name:           python-pep8
Version:        1.7.1
Release:        1
Summary:        Python style guide checker
License:        MIT
URL:            http://pep8.readthedocs.org/
Source0:        https://files.pythonhosted.org/packages/01/a0/64ba19519db49e4094d82599412a9660dee8c26a7addbbb1bf17927ceefe/pep8-1.7.1.tar.gz
BuildArch:      noarch
%description
pep8 Python style guide checker pep8 is a tool to check your Python code against
some of the style conventions in PEP 8... _PEP 8: architecture: Adding new
checks is easy.Parseable output: Jump to error location in your editor.Small:
Just one Python file, requires only stdlib. You can use just the pep8.py file
for this purpose.Comes with a comprehensive test suite.Installation You can
install, upgrade, uninstall pep8.py with these commands:: $ pip install pep8 $
pip install --upgrade pep8 $ pip uninstall pep8There's also a package for
Debian/Ubuntu, but it's not always the latest version.

%package -n python3-pep8
Summary:        Python style guide checker
Provides:       python-pep8
# Base build requires
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pbr
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
%description -n python3-pep8
%{common_desc}

%package help
Summary:        Python style guide checker
Provides:       python3-pep8-doc
%description help
%{common_desc}

%prep
%autosetup -n pep8-%{version}

%build
%py3_build


%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
    find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
    find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
    find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
    find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
    find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%check
# %{__python3} setup.py test

%files -n python3-pep8 -f filelist.lst
%dir %{python3_sitelib}/*


%files help -f doclist.lst
%{_docdir}/*

%changelog
* Tue Dec 06 2022 jiangxinyu <jiangxinyu@kylinos.cn> - 1.7.1-1
- Upgrade package to version 1.7.1

* Tue Aug 3 2021 huangtianhua <huangtianhua@huawei.com> - 1.5.7-2
- Correct licence

* Tue Jul 20 2021 OpenStack_SIG <openstack@openeuler.org> - 1.5.7-1
- Package Spec generate
